﻿using PropertyChanged;

namespace WPFTemplate
{
    /// <summary>
    /// 配置项
    /// </summary>
    [AddINotifyPropertyChangedInterface]
    public class ConfigItems:WPFTemplateLib.ConfigItems
    {
        /// <summary>
        /// 服务器地址
        /// </summary>
        public string Url { get; set; } = "192.168.16.120";

        /// <summary>
        /// 文件夹路径
        /// </summary>
        public string FolderPath { get; set; }
    }
}
