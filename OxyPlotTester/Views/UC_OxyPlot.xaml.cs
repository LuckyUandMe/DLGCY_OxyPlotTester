﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OxyPlot;

namespace OxyPlotTester.Views
{
    /// <summary>
    /// UC_OxyPlot.xaml 的交互逻辑
    /// </summary>
    public partial class UC_OxyPlot : UserControl
    {
        public UC_OxyPlot()
        {
            InitializeComponent();

            //自定义控制器;
            var myController = new PlotController();
            _PlotView.Controller = myController;
            //绑定鼠标进入（移上）事件到显示 Tracker 命令;
            //myController.BindMouseEnter(PlotCommands.HoverTrack);
            myController.BindMouseEnter(PlotCommands.HoverPointsOnlyTrack);

            //禁用鼠标右键调用 Pan 命令;
            myController.BindMouseDown(OxyMouseButton.Right, OxyModifierKeys.None, null);
        }
    }
}
