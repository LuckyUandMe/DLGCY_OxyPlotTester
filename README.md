# DLGCY_OxyPlotTester

## 介绍

WPF 图表控件 OxyPlot 测试程序

## 相关文章

- 《[OxyPlot.Wpf 图表控件使用备忘](http://dlgcy.com/oxyplot-wpf-use-way/)》
- 《[OxyPlot.WPF 公共属性一览](http://dlgcy.com/oxyplot-wpf-public-props/)》

## 图片

![截图](./Images/Snipaste_2022-02-18_15-53-19.png)
